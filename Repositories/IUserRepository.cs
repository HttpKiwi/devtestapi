using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DevTest;

namespace DevTest.Repositories
{
    public interface IUserRepository
    {
        Task<User> AddUser(User user);
        Task<IEnumerable<User>> GetUsers();
        Task<User> GetUser(Guid userId);
        Task<User> EditUser(User user);
        Task DeleteUser(Guid userId);
    }
}