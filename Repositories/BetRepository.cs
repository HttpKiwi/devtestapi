using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace DevTest.Repositories
{
    public class BetRepository : IBetRepository
    {
        private readonly IBetContext _context;
        public BetRepository(IBetContext context)
        {
            _context = context;
        }
        public async Task AddBet(Bet bet)
        {
            _context.Bets.Add(bet);
            await _context.SaveChangesAsync(); 
        }

        public async Task DeleteBet(Guid id)
        {
            var toDelete = await _context.Bets.FindAsync(id);
            Console.WriteLine(toDelete);
            if(toDelete == null)
                throw new NullReferenceException();

            _context.Bets.Remove(toDelete);
            await _context.SaveChangesAsync();
        }

        public async Task EditBet(Bet bet)
        {
            var toUpdate = await _context.Bets.FindAsync(bet.BetId);
            if (toUpdate == null)
                throw new NullReferenceException();
            
            toUpdate.BetAmount = bet.BetAmount;
            await _context.SaveChangesAsync();
        }

        public async Task<Bet> GetBet(Guid id)
        {
            return await _context.Bets.FindAsync(id); 
        }

        public async Task<IEnumerable<Bet>> GetBets()
        {
           return await _context.Bets.ToListAsync();
        }
    }
}