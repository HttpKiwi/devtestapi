using System;
using Microsoft.AspNetCore.Mvc;
using DevTest.Repositories;
using System.Threading.Tasks;

namespace DevTest.Controllers
{
    [ApiController]
    [Route("api/user")]
    
    public class UserControllers : ControllerBase
    {
        private IUserRepository _userRepository;

        public UserControllers(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }
        //Create
        [HttpPost]
        public IActionResult AddUser(User user)
        {
            _userRepository.AddUser(user);
            return Ok($"User with ID {user.UserId} added successfully!");
        }
        //Research
        [HttpGet]
        public async Task<IActionResult> GetUsersAsync()
        {
           return Ok(await _userRepository.GetUsers());
        }
        [HttpGet("{userId}")]
        public async Task<IActionResult> GetUserAsync(Guid userId)
        {
            User user = await _userRepository.GetUser(userId);
            if(user != null)
            {
                return Ok(user);
            }
            return NotFound($"user {userId} not found");
        }
        //Update
        [HttpPut]
        public async Task<IActionResult> UpdateUserAsync(User user)
        {
            User userFound = await _userRepository.GetUser(user.UserId);
            if(userFound != null)
            {
                user.UserId = userFound.UserId;
                await _userRepository.EditUser(user);
            }
            return Ok($"User with ID {user.UserId} has the name {user.UserName} now!" );
        }
        //Delete
        [HttpDelete("{userId}")]
        public async Task<IActionResult> DeleteUserAsync(Guid userId)
        {
            User user = await _userRepository.GetUser(userId);

            if(user != null)
            {
                await _userRepository.DeleteUser(user.UserId);
                return Ok($"User Deleted");
            }
            return NotFound($"The user with ID {userId} does not exist" );
        }
    }
}