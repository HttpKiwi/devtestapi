using System;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using DevTest.Repositories;
using System.Threading.Tasks;

namespace DevTest.Controllers
{
    [ApiController]
    [Route("api/game")]
    
    public class GameControllers : ControllerBase
    {
        private IGameRepository _gameRepository;
        public GameControllers(IGameRepository gameRepository)
        {
           _gameRepository = gameRepository;
        }

        [HttpPost]
        public async Task<IActionResult> AddGameAsync(Game game)
        {
           Game ng = await _gameRepository.AddGame(game);
           return Ok($"Game with ID {ng.GameId} added successfully!");
        }
        [HttpGet]
        public IActionResult GetGames()
        {
           return Ok(_gameRepository.GetGames());
        }
        [HttpGet("{gameId}")]
        public async Task<IActionResult> GetGameAsync(Guid gameId)
        {
           Game gID = await _gameRepository.GetGame(gameId);
           return Ok($"The teams of the game with ID {gameId} are {gID.GameHostTeam} and {gID.GameGuestTeam}");
        }
        [HttpPut]
        public async Task<IActionResult> EditGameAsync(Game game)
        {
            Game gameF = await _gameRepository.EditGame(game);
            return Ok($"Game Edited Succesfully" );
        }
        [HttpDelete("{gameId}")]
        public async Task<IActionResult> DeleteGameAsync(Guid gameId)
        {
            await _gameRepository.DeleteGame(gameId);
            return Ok($"The game with ID {gameId} has been deleted" );
        }
    }
}